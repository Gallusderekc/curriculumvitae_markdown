# Ramses Sanchez Barragan

**Date of birth:** 31/12/1993<br>
**Nationality:** Spanish<br>
**Adress:** Av. Igualada 5, 2d 3th, 08784, Piera, Barcelona, Spain<br>
**Phone number:** +34 651 112 515<br>
**Email adress:** cacaillo93@gmail.com<br>
---
##  `Work Experience`
10/11/2012 – 17/03/2013<br>
Barcelona, Spain
#### Adversiting deliveryman
**Reformas Sant Marti**<br>
- Distributed advertising around Barcelona.<br>


06/03/2010 – 12/08/2012<br>
Barcelona, Spain<br>
#### Maintenance assistant
**Reformas Davmar**<br>
- Stock managment. Electricity, gas and 
- water home installation

##  `Education`
16/09/2010 – 09/06/2011<br>
Barcelona, Spain
#### [IES Rambla Prim]( https://agora.xtec.cat/iesramblaprim/)
**Vocational Training (PQPI) Assistant Technician of Water, electricity and gas installations**
- Electricity, gas and water home installation and mantainer

10/09/2018 – Current<br>
Barcelona, Spain
#### [Institut Tecnològic de Barcelona](https://www.itb.cat/)
**Vocational training (CFGS) Cross-platform App development (Videogames & Digital Entertainment)**
- programing with Python, Java, Kotlin and C#
- Developing Android aplications with Android Studio
- Developing videogames with Unity
- 2D/3D dessign with Blender
